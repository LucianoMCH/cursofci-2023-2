import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp


class soledEuler:
    '''
    Clase para encontrar la solucion a una ecuacion diferencial de primer orden con el metodo de euler

    a: Extremo inferior del intervalo en x. Número real, int o float
    b: Extremo superior del intervalo en x. Número real, int o float
    f: Lado derecho de la ecuación y(x) = f(x,y). Función de dos variables.
    y0: Valor inicial de y, de la condición y(a) = y0. Número real, int o float
    xp: punto distinto de b en el que se quiere evaluar la función. Número real, int o float
    N: Tamaño del intervalo en que se tomará el conjunto entre [a,b]. Entero, de tipo int

    '''
    def __init__(self,a,b,f,y0,xp=False,N=1500):
        self.funcion = f
        self.xmin = a
        self.xmax = b
        self.yi = y0
        self.xp = xp
        self.num = N
        self.ey = []
        self.rky = []

    def paso(self): # paso en el que se calcula la solucion
        try:
            if self.xp: # si se define el punto
                return (self.xp - self.xmin)/self.num
            else: # si solo se define el intervalo
                return (self.xmax - self.xmin)/self.num
        except:
            print('Revise que los parámetros de entrada cumplan con las condiciones establecidas')
            raise SystemExit(0)
        
    def ex(self): # arreglo con los puntos en los que se va a evaluar la solución Y
        try:
            if self.xp: # si se define el punto
                return np.arange(self.xmin,self.xp+self.paso(),self.paso())
            else: # si solo se define el intervalo
                return np.arange(self.xmin,self.xmax+self.paso(),self.paso())
        except:
            print('Revise que los parámetros de entrada cumplan con las condiciones establecidas')
            raise SystemExit(0)

    def sol_ana(self): # solucion "analítica" al problema
        try:
            if self.xp: # si se define el punto
                sol = solve_ivp(self.funcion, [self.xmin, self.xp], [self.yi], rtol = 1e-5)
            else: # si solo se define el intervalo
                sol = solve_ivp(self.funcion, [self.xmin, self.xmax], [self.yi], rtol = 1e-5)
            return sol
        except:
            print('Revise que los parámetros de entrada cumplan con las condiciones establecidas')
            raise SystemExit(0)

    def sol_eu(self): # solución con el método de euler
        try:
            self.ey = np.zeros(self.num+1)
            self.ey[0] = self.yi
            for i in range(self.num):
                self.ey[i+1] = self.ey[i]+self.paso()*self.funcion(self.ex()[i],self.ey[i])
            return self.ey
        except:
            print('Revise que los parámetros de entrada cumplan con las condiciones establecidas')
            raise SystemExit(0)
    
    def sol_rk4(self): # solución con el método de Runge-Kutta de orden 4
        try:
            self.rky = np.zeros(self.num+1) 
            self.rky[0] = self.yi
            for i in range(self.num):
                k1 = self.funcion(self.ex()[i],self.rky[i])
                k2 = self.funcion(self.ex()[i]+0.5*self.paso(),self.rky[i]+0.5*k1*self.paso())
                k3 = self.funcion(self.ex()[i]+0.5*self.paso(),self.rky[i]+0.5*k2*self.paso())
                k4 = self.funcion(self.ex()[i]+self.paso(),self.rky[i]+k3*self.paso())

                self.rky[i+1] = self.rky[i] + ( self.paso()/6 )*(k1 + 2*k2 + 2*k3 + k4)
            return self.rky
        except:
            print('Revise que los parámetros de entrada cumplan con las condiciones establecidas')
            raise SystemExit(0)

    # manejo de errores

    def imp(self): # Se imprime el resultado de la solución para Y en el punto dado 
        try:
            if self.xp: # si se define el punto
                px = self.xp
            else: # si sólo se define el intervalo se evalúa en el límite superior
                px = self.xmax
            print( f'\nEn el punto x={px}: \ncon la sol de euler y = {round(self.sol_eu()[-1])} \ncon \
                la solucion analitica y = {round(self.sol_ana().y[0][-1])}\ncon RK4: \
                y = {round(self.sol_rk4()[-1])}')
        except:
            print('Revise que los parámetros de entrada cumplan con las condiciones establecidas')
            raise SystemExit(0)

    def graf_xy(self): # Gráfica X vs Y de la solución usando los distintos métodos
        try:
            plt.figure()
            plt.grid()
            plt.title('Gráfica de las soluciones')
            plt.plot(self.ex(),self.sol_eu(),'-',c='r', label='Método de Euler')
            plt.plot(self.ex(),self.sol_rk4(),'-.',c='g',label='Solución RK4')
            plt.plot(self.sol_ana().t,self.sol_ana().y[0],':',c='k',label='Analítica')
            plt.legend()
            plt.xlabel('X')
            plt.ylabel('Y')
            plt.show()
        except:
            print('Revise que los parámetros de entrada cumplan con las condiciones establecidas')
            raise SystemExit(0)