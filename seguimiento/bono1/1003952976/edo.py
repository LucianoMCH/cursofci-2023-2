import numpy as np
#from sympy import*
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt


class edo_solution:

    '''Clase con métodospara resolver ecuaciones diferenciales
    oredinarias. método de Euler, método Runge Kutta 4 y método analítico'''

    def __init__(self,a,b,n,y0,f,punto=False): #punto=False no se usa acá, ya que si
                                               #encontrar el  valor de la función en un punto 
                                               #particular, solo necesitamos usar un intervalo 
                                               #[a,b] apropiado

        '''ingrese las variables, parámetros o funciones en el orden
        correcto'''

        self.a=a
        self.b=b
        self.n=n
        self.y0=y0
        self.punto=punto
        self.x=[]
        self.y=[]
        self.f=f

    
    def h(self):

        '''se procede a crear el paso con el que avanza 
        la solución de la edo'''

        if self.punto:
            return(self.punto-self.a)/self.n
        else:
            return(self.b-self.a)/self.n

    def X(self):

        '''se crea el array para los valores de x, iniciando
        en a y terminando en b'''

        if self.punto:
            self.x=np.arange(self.a,self.punto+self.h(),self.h())
        else:
            self.x=np.arange(self.a,self.b+self.h(),self.h())
        
        return self.x
    
    def euler(self):

        '''algoritmo de Euler para la solución de de edo'''

        self.X()

        self.y=[self.y0]
        for i in range(self.n):
            self.y.append(self.y[i]+self.h()\
                *self.f(self.x[i],self.y[i]))
        
        #poner la condición del punto

        return self.x, self.y

    def rk4(self):

        '''algortimo runge-kutta 4 para edo'''

        self.X()

        self.y=[self.y0]
        for i in range(self.n):
            self.k1=self.h()*self.f(self.x[i],self.y[i])
            self.k2=self.h()*self.f(self.x[i]+self.h()/2,self.y[i]+self.k1/2)
            self.k3=self.h()*self.f(self.x[i]+self.h()/2,self.y[i]+self.k2/2)
            self.k4=self.h()*self.f(self.x[i]+self.h(),self.y[i]+self.k3)

            self.y.append(self.y[i]+(1/6)*(self.k1+2*self.k2\
                +2*self.k3+self.k4))
        
        return self.x , self.y
    
    def solanalitica(self):

        self.X()

        self.y=np.array([self.y0])
        self.inter=np.array([self.a,self.b])
        self.sol=solve_ivp(self.f,self.inter,self.y,t_eval=self.x)
        return self.sol.t, self.sol.y[0]

    def figplot(self):

        '''gráfica de la solución con los distintos métodos'''

        plt.figure(figsize=(10,6))

        xrk4,yrk4=self.rk4() 
        xeuler,yeuler=self.euler()
        xana,yana=self.solanalitica()

        plt.plot(xeuler,yeuler,label="sol_euler")
        plt.plot(xrk4,yrk4,label="sol_rk4")
        plt.plot(xana,yana,label="sol_analitica")

        plt.legend()
        plt.title("gráfica de la función solución a la ecuación diferencial")
        plt.savefig("solución.png")

