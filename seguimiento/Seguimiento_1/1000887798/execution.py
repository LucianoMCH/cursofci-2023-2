from par_cargada import particula
import sys


"""
NOTA

5.0

- Muy buen trabajo

"""


if not sys.warnoptions:      
    import warnings      #Esto es para que no salgan los warnings
    warnings.simplefilter("ignore")

if __name__ == "__main__":
    print("Funcionando")


    #Definir entradas
    Ek = 0.3  #eV
    theta = 40  #grados
    m = 9.10938356e-31  #kg
    B = 0#700e-0  #T

    #Instanciar la clase
    particula1 = particula(Ek,theta,m,B)
    

    #Se utilizan los métodos de la clase
    print("La velocidad de la particula es: ", round(particula1.vel(),2))
    print("La frecuencia ciclotronica es: ", particula1.W())
    print(f"La velocidad en cada componente es: {particula1.velc()} " )   
    print(f"La posición en cada componente es: {particula1.posc(0.01)} " )

    #Se grafica la trayectoria de la particula para un resultado diciente
    particula1.grafica(.0000002,10000)

    #Se grafica la trayectoria de la particula para las condiciones sugeridas  
    #Se deja comentado,pero se sube el archivo de la gráfica
    
    #particula1.grafica(0.01,1000)

    

    
