from fuerza_lorentz import campo_magnetico


"""
NOTA

5.0

- Muy buen trabajo

"""

if __name__ =="__main__":

    Ek=0#18.06 #eV
    theta=0 #º
    B=0#600e-6 #microteslas
    m=9.1e-31 #Kg - aproximadamente
    q=-1.6e-19 #C

    T=[100,0.001] #[iteraciones,paso] #para que se vea mejor la gáfica ponga [70,0.002]

    solucion=campo_magnetico(Ek,theta,B,m,q,T)
    solucion.graficar()
    solucion.run()