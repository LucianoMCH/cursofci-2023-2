from Particula import ParticulalEnCampoMagnetico

"""
NOTA

5.0

- Muy buen trabajo

"""

if __name__ == "__main__":
    #Nota:
    #La region del campo magnetico esta sobre la parte positiva del eje Y (Y>0)
    # La posicion inicial de la particula debe estar fuera de la region con campo magnetico, es decir, Y<0.
    
    
    #Parametros de entrada
    Ek    = 1           #eV; energia cinetica
    theta = 1                #Grados; 0< theta<180
    phi   = 45                 #Grados; 0< phi<180
    m     = 9.10938e-31        #kg; masa del electron
    B     = 600                #microT; campo magnetico
    q     = -1.60217662e-19    #C; carga del electron
    x0    = [0,-0.01,0]        #posicion inicial [x,y,z]
    t_final = 1e-5        #s; tiempo final de simulacion
    
    
    #Instancia de la clase Particula
    particula1 = ParticulalEnCampoMagnetico(Ek,theta,phi,m,B,q,x0,t_final)
    particula1.run()