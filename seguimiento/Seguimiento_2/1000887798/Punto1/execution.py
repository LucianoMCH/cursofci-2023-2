from montecarlo import Integral_montecarlo 
import numpy as np
import sympy as sp

if __name__ == "__main__":
    
    # Prueba de la clase Integral_montecarlo
    # Se definen los parametros de entrada de la clase

    a  = 0
    b =1# np.pi
    f = lambda x: sp.tanh(x/7)#x**2 * sp.cos(x)

    # n = 35 ##Numero de iteraciones para generar el plot "montecarlo.png"
    n =  100000  #Valor alto para obtener mejor apróximación de la integral

    #Se instancia la clase Integral_montecarlo
    integral = Integral_montecarlo(f, a, b, n)  #Se instancia la clase

    print(f"El valor por el método montecarlo es : {integral.area(integral.n):.3}")
    print(f"El valor por el método analítico es : {integral.analitica():.3}")


    #Descomentar par generar "montecarlo.png"
    #integral.plot() ##Plot analitica vs montecarlo para n = 35 se ve mejor 
    

    #Plot iteraciones 
    integral.plot_iteraciones(10, 10000)

    


    
    